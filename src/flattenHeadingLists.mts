// Copyright 2024, Collabora, Ltd.
//
// SPDX-License-Identifier: GPL-3.0-only

// Original author: Rylie Pavlik <rylie.pavlik@collabora.com>

import { visit } from "unist-util-visit";
import { List, Node, ListItem, Root, Parent } from "mdast";
import { is } from "unist-util-is";

import "unified";

const unorderedListTemplate = { type: "list", ordered: false };

const allListItemChildrenAreListsOrHeadings = (li: ListItem) =>
  li.children.every((n) => is(n, "heading") || is(n, "list"));

// we want a list with some listItems, and arbitrary numbers of children in each list item as long as they are headings or lists.

const isTargetList = (node: Node) => {
  return (
    is(node, unorderedListTemplate) &&
    (node as List).children.length >= 1 &&
    (node as List).children.every(allListItemChildrenAreListsOrHeadings)
  );
};

const getFlattenedChildren = (node: List) => {
  if (node.children.length === 1) {
    return node.children[0].children;
  }
  return node.children.flatMap((li: ListItem) => li.children);
};

export const transform = () => (tree: any) => {
  const visitor = (node: Node, index, parent: Parent) => {
    if (isTargetList(node)) {
      console.log("Replacing a child at index", index);
      var p = parent as Root | ListItem;
      p.children = p.children
        .slice(0, index)
        .concat(getFlattenedChildren(node as List))
        .concat(p.children.slice(index + 1));
    }
  };
  // handle the root
  visitor(tree.children[0], 0, tree);

  // now handle beyond the root
  visit(tree, (node, index, parent: Parent) => {
    visitor(node, index, parent);
  });
};

export const flattenHeadingLists = () => {
  return transform();
};
