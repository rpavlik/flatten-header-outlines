// Copyright 2024, Collabora, Ltd.
//
// SPDX-License-Identifier: GPL-3.0-only

// Original author: Rylie Pavlik <rylie.pavlik@collabora.com>

import { visit } from "unist-util-visit";

export interface IOptions {
  raiseHeadings: number;
}
export const transform = (opts: IOptions) => (tree: any) => {
  const visitor = (node: any) => {
    node.depth = node.depth - opts.raiseHeadings;
  };
  visit(tree, "heading", visitor);
};

export const headingRaiser = (opts: IOptions) => {
  return transform(opts);
};
