#!/usr/bin/env node
// Copyright 2024, Collabora, Ltd.
//
// SPDX-License-Identifier: GPL-3.0-only

// Original author: Rylie Pavlik <rylie.pavlik@collabora.com>

import { unified } from "unified";
import remarkParse from "remark-parse";
import fs from "node:fs/promises";
import remarkStringify from "remark-stringify";
import { headingRaiser } from "./raiseHeadings.mjs";
import { flattenHeadingLists } from "./flattenHeadingLists.mjs";
import { program } from "commander";
import remarkNormalizeHeadings from "remark-normalize-headings";

const applyTransform = async (processor, pathIn, pathOut) => {
  const input = String(await fs.readFile(pathIn));

  const ast = processor.parse(input);

  const transformed = await processor.run(ast);

  const result = processor.stringify(transformed as any);

  await fs.writeFile(pathOut, result);
};

program
  .argument("<input_file>", "Input markdown file as exported from LogSeq")
  .argument(
    "[output_file]",
    "Output filename. If not provided, will overwrite the input."
  )
  .option(
    "-r, --raise <levels>",
    "how many heading levels to raise headings.",
    "1"
  )
  .option(
    "-n, --normalize",
    "Normalize heading levels so exactly one top level heading exists."
  )
  .action((inputFile, outputFile, opts) => {
    const levels = parseInt(opts.raise);
    if (outputFile === undefined) {
      outputFile = inputFile;
    }
    let proc = unified()
      .use(remarkParse)
      .use(headingRaiser, { raiseHeadings: levels })
      .use(flattenHeadingLists)
      .use(remarkStringify, { bullet: "-" });
    if (opts.normalize) {
      proc.use(remarkNormalizeHeadings);
    }
    applyTransform(proc, inputFile, outputFile);
  });

program.parse();
